# GNTTools plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

```
composer require stefanmayr/cakephp-gnttools
```

Manually add by git (within the root directory of your cakephp app):

```
git submodule add git@gitlab.grizzlyserver.com:worldshaking-server/cakephp-gnttools.git plugins/GNTTools
```

and add this to your bootstrap.php

```
$this->addPlugin(GNTTools::class, ['bootstrap' => false, 'routes' => true]);
```