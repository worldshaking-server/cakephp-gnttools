<?php

namespace GNTTools\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Mailer\TransportFactory;

class MailComponent extends Component {

    public function send($form, $upload) {
        $mailsettings = $this->getMailSettings($form->mailsettings_id);
        if (!empty($mailsettings)) {
            $email = $this->__initEmail($mailsettings, $upload->email);
            $email->setEmailFormat('html');
            $email->viewBuilder()->setTemplate('verify');
            $email->viewBuilder()->setLayout('template1');
            $linkdomain = !empty($form->domain) ? $form->domain : "upload.snapshotboard.com";
            $email->setViewVars(['mailtext' => $form->mailtext, 'mailpreviewtext' => $form->mailpreviewtext, 'key' => $upload->authorizationkey, "linkdomain" => $linkdomain, "domain" => $domain, "slug" => $form->slug]);
            $email->setSubject($form->mailsubject)->send();
        }
    }

    private function __initEmail($mailsettings, $recipient) {
        $this->__setConfig($mailsettings);
        $email = new Email();
        $sender = $mailsettings->senderemail;
        $senderName = $mailsettings->sendername;
        $domain = $mailsettings->senderdomain;
        $message_id = "<" . time() . "-" . md5($sender . $recipient) . '@' . $domain . ">";
        $email->setFrom([$sender => $senderName]);
        $email->setTransport("custom");
        $email->setMessageId($message_id);
        $email->setTo($recipient);
        return $email;
    }

    private function __setConfig($mailsettings) {
        TransportFactory::setConfig('custom', [
            'host' => $mailsettings->host,
            'port' => $mailsettings->port,
            'timeout' => 5,
            'username' => $mailsettings->username,
            'password' => $mailsettings->password,
            'className' => 'Smtp',
            'tls' => $mailsettings->tls,
            'client' => $mailsettings->senderdomain
        ]);
    }

    public function createEntityFromFormData($data, $user_id) {
        $entity = $this->MailSettings->newEntity($data);
        $entity->user_id = $user_id;
        if (!empty($entity->senderemail)) {
            $entity->senderdomain = substr($entity->senderemail, strpos($entity->senderemail, '@') + 1);
        }
        return $entity;
    }

    public function saveSettings($entity) {
        return $this->MailSettings->save($entity);
    }

    public function getAllMailSettings($user_id) {
        return $this->MailSettings->find()->where(["user_id" => $user_id]);
    }

    public function getMailSettings($id) {
        return $this->MailSettings->find()->where(["id" => $id])->limit(1)->first();
    }

    public function deleteSettings($entity) {
        return $this->MailSettings->delete($entity);
    }

    public function initialize(array $config) {
        parent::initialize($config);
        if (empty($this->MailSettings)) {
            $this->MailSettings = TableRegistry::get('Mailsettings');
        }
    }

}
