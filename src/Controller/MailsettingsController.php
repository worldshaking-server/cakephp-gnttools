<?php

namespace GNTTools\Controller;

use GNTTools\Controller\AppController;

/**
 * Mailsettings Controller
 *
 *
 * @method \GNTTools\Model\Entity\Mailsetting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MailsettingsController extends AppController {

    public function index() {
        $this->set("PAGE", ["title" => __("Mail-Settings")]);
        $mailsettings = $this->Mail->getAllMailSettings($this->Auth->user("id"));
        $this->loadComponent('Paginator');
        $pagination_subscriptions = $this->Paginator->paginate($mailsettings, ["limit" => 15, "order" => ["senderemail ASC"]]);
        $this->set("paginator", $this->Paginator->getPagingParams());
        $this->set("mailsettings", $pagination_subscriptions);
    }

    public function add() {
        $this->set("PAGE", ["title" => __("Add new Mail-Settings")]);
        if ($this->request->is("post")) {
            $entity = $this->Mail->createEntityFromFormData($this->request->getData(), $this->Auth->user("id"));
            $this->Mail->saveSettings($entity);
            return $this->redirect("/mailsettings");
        }
    }

    public function edit($id) {
        $this->set("PAGE", ["title" => __("Edit Mail-Settings")]);
        $mailsetting = $this->Mail->getMailSettings($id);
        $this->set("mailsetting", $mailsetting);
        return $this->render('add');
    }

    public function remove($id) {
        $mailsettings = $this->Mail->getMailSettings($id);
        if (!empty($mailsettings)) {
            if ($mailsettings->user_id == $this->Auth->user("id")) {
                $this->Mail->deleteSettings($mailsettings);
            }
        }
        return $this->redirect("/mailsettings");
    }

    public function beforeFilter($event) {
        parent::beforeFilter($event);
        $this->loadComponent("GNTTools.Mail");
    }

}
