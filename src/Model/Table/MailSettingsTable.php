<?php

/*
 * 
  CREATE TABLE `mailsettings` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `senderemail` varchar(200) NOT NULL,
 `sendername` varchar(100) NOT NULL,
 `senderdomain` varchar(50) NOT NULL,
 `host` varchar(100) NOT NULL DEFAULT '',
 `port` int(11) NOT NULL DEFAULT '465',
 `username` varchar(100) NOT NULL DEFAULT '',
 `password` varchar(255) NOT NULL,
 `tls` tinyint(1) NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4
 * 
 */

namespace App\Model\Table;

use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Utility\Security;

class MailsettingsTable extends Table {

    public $encryptedFields = [
        'password'
    ];

    public function beforeSave($event, $entity, $options) {
        foreach ($this->encryptedFields as $fieldName) {
            if ($entity->has($fieldName)) {
                $entity->set(
                        $fieldName, base64_encode(Security::encrypt($entity->get($fieldName), Security::getSalt()))
                );
            }
        }
        return true;
    }

    public function beforeFind($event, $query, $options, $primary) {
        $query->formatResults(
                function ($results) {
            return $results->map(function ($row) {
                        foreach ($this->encryptedFields as $fieldName) {
                            if (isset($row[$fieldName]) && !empty($row[$fieldName])) {
                                $data = base64_decode($row[$fieldName]);
                                if (!empty($data)) {
                                    $row[$fieldName] = Security::decrypt(base64_decode($row[$fieldName]), Security::getSalt());
                                }
                            }
                        }
                        return $row;
                    });
        }
        );
    }

}
