<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xl-10" style="max-width:1024px;">
        <div class="card card-body">
            <div class="row" style="margin-top:20px;">
                <div class="col-sm-12 col-xs-12">
                    <?php $action = !empty($mailsetting) ? "edit" : "add"; $argument = !empty($mailsetting) ? $mailsetting->id : ""; echo $this->Form->create(null, ['url' => ['action' => $action, $argument], 'class' => "form-horizontal"]); ?>
                    <div class="form-body">
                        <h3 class="box-title"><?=__("Sender's information")?></h3>
                        <hr class="m-t-0 m-b-40">
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3"><?=__("Sender-Name")?></label>
                                    <div class="col-md-9">
                                        <input name="sendername" type="text" class="form-control" placeholder="" required value="<?=!empty($mailsetting->sendername)?$mailsetting->sendername:""?>">
                                    </div>
                                </div>
                            </div>                            
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3"><?=__("Email-Adress")?></label>
                                    <div class="col-md-9">
                                        <input name="senderemail" type="email" class="form-control" placeholder="" required value="<?=!empty($mailsetting->senderemail)?$mailsetting->senderemail:""?>">
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <h3 class="box-title"><?=__("SMTP-Server-Settings")?></h3>
                        <hr class="m-t-0 m-b-40">                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3"><?=__("SMTP-Host")?></label>
                                    <div class="col-md-9">
                                        <input name="host" type="text" class="form-control" placeholder="" required value="<?=!empty($mailsetting->host)?$mailsetting->host:""?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3"><?=__("SMTP-Port")?></label>
                                    <div class="col-md-9">
                                        <input name="port" type="text" class="form-control" placeholder="" required value="<?=!empty($mailsetting->port)?$mailsetting->port:""?>">
                                    </div>
                                </div>
                            </div>                            
                            <!--/span-->
                        </div>                             
                        <!--/row-->                            
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3"><?=__("User-Name")?></label>
                                    <div class="col-md-9">
                                        <input name="username" type="text" class="form-control" placeholder="" required value="<?=!empty($mailsetting->username)?$mailsetting->username:""?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3"><?=__("Password")?></label>
                                    <div class="col-md-9">
                                        <input name="password" type="password" class="form-control" placeholder="" <?=empty($mailsetting)?"required":""?>>
                                    </div>
                                </div>
                            </div>
                           
                            <!--/span-->
                        </div>                             
                        <!--/row-->                        
                    </div>
                    <hr>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                                        <a class="btn btn-dark" href="/subscriptions/manage">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"> </div>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>

                </div>
            </div>
        </div>
    </div>
</div>
