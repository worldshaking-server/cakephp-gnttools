<?php
$this->Paginator->setTemplates([
    'prevDisabled' => '<li class="footable-page-nav disabled" data-page="first"><a class="footable-page-link" href="{{url}}">‹</a></li>',
    'prevActive' => '<li class="footable-page-nav" data-page="first"><a class="footable-page-link" href="{{url}}">‹</a></li>',
    'nextDisabled' => '<li class="footable-page-nav disabled" data-page="first"><a class="footable-page-link" href="{{url}}">›</a></li>',
    'nextActive' => '<li class="footable-page-nav" data-page="first"><a class="footable-page-link" href="{{url}}">›</a></li>',
    'first' => '<li class="footable-page-nav" data-page="first"><a class="footable-page-link" href="{{url}}">«</a></li>',
    'last' => '<li class="footable-page-nav" data-page="first"><a class="footable-page-link" href="{{url}}">»</a></li>'
]);
?><div class="row">
    <div class="col-12">
        
            <a class="btn btn-info btn-rounded m-t-10 float-right" href="/tools/mailsettings/add"><?= __("Add new") ?></a>
            <div class="table-responsive">
                <table id="demo-foo-addrow" class="table no-wrap table-bordered m-t-30 table-hover contact-list footable footable-1 footable-paging footable-paging-center breakpoint-lg" data-paging="true" data-paging-size="7" style="">
                    <thead>
                        <tr class="footable-header">
                            <th style="display: table-cell;">Sender's name</th>
                            <th style="display: table-cell;">Email-Address</th>
                            <th style="display: table-cell;">Host</th>
                            <th style="display: table-cell;"><?= __("Actions") ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($mailsettings as $mailsetting) {
                            ?>
                            <tr>
                                <td style="display: table-cell;"><?= !empty($mailsetting["sendername"]) ? $mailsetting["sendername"] : "" ?></td>
                                <td style="display: table-cell;"><?= !empty($mailsetting["senderemail"]) ? $mailsetting["senderemail"] : "" ?></td>
                                <td style="display: table-cell;"><?= !empty($mailsetting["host"]) ? $mailsetting["host"] : "" ?></td>
                                <td style="display: table-cell;">
                                    <a href='/tools/mailsettings/edit/<?= $mailsetting->id ?>' class="sa-info btn waves-effect waves-light btn-outline-info"><?=__("Edit")?></a>
                                    <button data-val='<?= $mailsetting->id ?>' class="sa-warning btn waves-effect waves-light btn-outline-danger"><?=__("Remove")?></button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr class="footable-paging">
                            <td colspan="4">
                                <div class="footable-pagination-wrapper">
                                    <ul class="pagination justify-content-center">
                                        <?= $this->Paginator->first(); ?>
                                        <?= $this->Paginator->prev(); ?>
                                        <?= $this->Paginator->next(); ?>
                                        <?= $this->Paginator->last(); ?>
                                    </ul>
                                    <div class="divider"></div>
                                    <span class="label label-primary"><?= $paginator["Mailsettings"]["page"] ?> of <?= $paginator["Mailsettings"]["pageCount"] ?></span>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        
    </div>

    <script>

        document.addEventListener("DOMContentLoaded", function (event) {
            jQuery('.sa-warning').click(function () {
                var subid = $(this).data('val');
                swal({
                    title: "<?=__("Are You sure?")?>",
                    text: "<?=__("This cannot be undone!")?>",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location.href = "/tools/mailsettings/remove/" + subid;
                    }
                });
            });
        });

    </script>

